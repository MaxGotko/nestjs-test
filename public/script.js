function myFunc(id) {
  const commentText = document.getElementById('commentTextarea').value;
  const payload = { text: commentText };

  fetch(`/posts/${id}/comment`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify(payload),
  })
    .then(function () {
      location.reload();
    })
    .catch(function (res) {
      console.log(res);
    });
}
