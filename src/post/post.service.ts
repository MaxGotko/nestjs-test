import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PostEntity } from './post.entity';
import { CreatePostDto } from './dto/create-post.dto';
import { CategoryService } from '../category/category.service';
import { paginateResponse } from '../utils/paginateResponse';
import { IPagination } from '../interfaces/pagination.interface';
import { AuthorService } from '../autor/author.service';
import { PaginationDto } from '../dto/pagination.dto';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(PostEntity)
    private newsRepository: Repository<PostEntity>,
    private categoryService: CategoryService,
    private authorService: AuthorService,
  ) {}
  async create(post: CreatePostDto): Promise<PostEntity> {
    const category = await this.categoryService.findOne(post.categoryId);
    const author = await this.authorService.findOne(post.authorId);
    const newsPayload = new PostEntity();
    newsPayload.title = post.title;
    newsPayload.text = post.text;
    newsPayload.category = category; //TODO: check if category exists, if not throw error
    newsPayload.author = author; //TODO: check if category exists, if not throw error
    return this.newsRepository.save(newsPayload);
  }
  async findAll(pagination: PaginationDto): Promise<IPagination<PostEntity[]>> {
    const skip = (pagination.page - 1) * pagination.perPage;
    const data = await this.newsRepository.findAndCount({
      order: { createdAt: 'DESC' },
      take: pagination.perPage,
      skip: skip,
    });
    return paginateResponse(data, +pagination.page, +pagination.perPage);
  }

  findOne(id: number): Promise<PostEntity | null> {
    return this.newsRepository.findOneBy({ id });
  }

  async remove(id: number): Promise<void> {
    await this.newsRepository.delete(id);
  }
}
