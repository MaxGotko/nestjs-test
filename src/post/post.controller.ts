import {
  Controller,
  Get,
  Post,
  Delete,
  Body,
  UsePipes,
  ValidationPipe,
  Param,
} from '@nestjs/common';
import { PostService } from './post.service';
import { PostEntity } from './post.entity';
import { CreatePostDto } from './dto/create-post.dto';
import { IPagination } from '../interfaces/pagination.interface';
import { CreateCommentDto } from '../comment/dto/create-comment.dto';
import { CommentService } from '../comment/comment.service';
import { CommentEntity } from '../comment/comment.entity';

@Controller('posts')
export class PostController {
  constructor(
    private postService: PostService,
    private commentService: CommentService,
  ) {}
  @Post()
  @UsePipes(new ValidationPipe())
  async create(@Body() createPostDto: CreatePostDto) {
    await this.postService.create(createPostDto);
  }

  @Get()
  async findAll(query): Promise<IPagination<PostEntity[]>> {
    return await this.postService.findAll(query);
  }
  @Post(':id/comment')
  @UsePipes(new ValidationPipe())
  async createComment(
    @Body() createCommentDto: CreateCommentDto,
    @Param('id') postId: number,
  ) {
    const post = await this.postService.findOne(postId);
    const comment = new CommentEntity();
    comment.text = createCommentDto.text;
    comment.post = post;
    await this.commentService.create(comment);
  }
  @Delete(':id')
  async remove(@Param() params): Promise<void> {
    return await this.postService.remove(params.id);
  }
}
