import { ICategory } from '../category/category.interface';

export interface IPost {
  id: number;
  title: string;
  text: string;
  createdAt: string;
  category: ICategory;
}
