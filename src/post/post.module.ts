import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { PostEntity } from './post.entity';
import { CategoryModule } from '../category/category.module';
import { AuthorModule } from '../autor/author.module';
import { CommentModule } from '../comment/comment.module';
@Module({
  imports: [
    TypeOrmModule.forFeature([PostEntity]),
    CategoryModule,
    AuthorModule,
    CommentModule,
  ],
  controllers: [PostController],
  providers: [PostService],
  exports: [PostService],
})
export class PostModule {}
