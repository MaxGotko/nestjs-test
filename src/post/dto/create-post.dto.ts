import { IsString, IsNotEmpty, IsInt } from 'class-validator';
export class CreatePostDto {
  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsNotEmpty()
  text: string;

  @IsNotEmpty()
  @IsInt() //TODO: check if categoryId exist in DB
  categoryId: number;

  @IsNotEmpty()
  @IsInt() //TODO: check if categoryId exist in DB
  authorId: number;
}
