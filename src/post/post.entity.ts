import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  CreateDateColumn,
  OneToMany,
} from 'typeorm';
import { Category } from '../category/category.entity';
import { AuthorEntity } from '../autor/author.entity';
import { CommentEntity } from '../comment/comment.entity';

@Entity('posts')
export class PostEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  text: string;

  @CreateDateColumn()
  createdAt: Date;

  @ManyToOne(() => Category, (category) => category.posts, {
    eager: true,
  })
  category: Category;

  @ManyToOne(() => AuthorEntity, (author) => author.posts, {
    eager: true,
  })
  author: AuthorEntity;

  @OneToMany(() => CommentEntity, (comment) => comment.post, {
    eager: true,
  })
  comments: CommentEntity[];
}
