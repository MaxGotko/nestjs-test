export interface IComment {
  id: number;
  text: string;
  author: string;
}
