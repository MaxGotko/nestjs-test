import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { DataSource } from 'typeorm';
import { PostModule } from './post/post.module';
import { PostEntity } from './post/post.entity';
import { CategoryModule } from './category/category.module';
import { Category } from './category/category.entity';
import { AuthorModule } from './autor/author.module';
import { AuthorEntity } from './autor/author.entity';
import { CommentModule } from './comment/comment.module';
import { CommentEntity } from './comment/comment.entity';
import { CacheModule } from '@nestjs/cache-manager';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      // TODO: move to config file
      type: 'mysql',
      host: '127.0.0.1',
      port: 3306,
      username: 'root',
      password: '12345678',
      database: 'nestjstest',
      entities: [PostEntity, Category, AuthorEntity, CommentEntity],
      synchronize: true,
    }),
    PostModule,
    CategoryModule,
    AuthorModule,
    CommentModule,
    CacheModule.register(),
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
