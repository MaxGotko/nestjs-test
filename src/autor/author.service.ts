import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AuthorEntity } from './author.entity';
import { CreateAuthorDto } from './dto/create-author.dto';
import { PostEntity } from "../post/post.entity";

@Injectable()
export class AuthorService {
  constructor(
    @InjectRepository(AuthorEntity)
    private authorRepository: Repository<AuthorEntity>,
  ) {}
  create(category: CreateAuthorDto): Promise<AuthorEntity> {
    return this.authorRepository.save(category);
  }
  findOne(authorId: number): Promise<AuthorEntity> {
    return this.authorRepository.findOneBy({ id: authorId });
  }
  async findNewsByAuthorId(id: number): Promise<PostEntity[]> {
    const category = await this.authorRepository.findOne({
      where: { id: id },
      relations: ['posts'],
    });
    if (!category) {
      throw new Error(`Author with ID ${id} not found`);
    }
    return category.posts;
  }
}
