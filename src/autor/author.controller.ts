import {
  Body,
  Controller,
  Post,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthorService } from './author.service';
import { CreateAuthorDto } from './dto/create-author.dto';

@Controller('author')
export class AuthorController {
  constructor(private authorService: AuthorService) {}
  @Post()
  @UsePipes(new ValidationPipe())
  async create(@Body() createAuthorDto: CreateAuthorDto) {
    await this.authorService.create(createAuthorDto);
  }
}
