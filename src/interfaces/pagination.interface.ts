export interface IPagination<T> {
  data: T[];
  pagination: {
    totalCount: number;
    page: number;
    perPage: number;
    nextPage: number;
    prevPage: number;
    lastPage: number;
  };
}
