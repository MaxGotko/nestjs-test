import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from './category.entity';
import { Repository } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import { IPagination } from '../interfaces/pagination.interface';
import { paginateResponse } from '../utils/paginateResponse';
import { PostEntity } from '../post/post.entity';
import { PaginationDto } from '../dto/pagination.dto';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>,
  ) {}

  async findAll(pagination: PaginationDto): Promise<IPagination<Category[]>> {
    const skip = (pagination.page - 1) * pagination.perPage;
    const data = await this.categoryRepository.findAndCount({
      take: pagination.perPage,
      skip: skip,
    });
    return paginateResponse(data, +pagination.page, +pagination.perPage);
  }
  create(category: CreateCategoryDto): Promise<Category> {
    return this.categoryRepository.save(category);
  }
  findOne(categoryId: number): Promise<Category> {
    return this.categoryRepository.findOneBy({ id: categoryId });
  }

  async findNewsByCategoryId(id: number): Promise<PostEntity[]> {
    const category = await this.categoryRepository.findOne({
      where: { id: id },
      relations: ['posts'],
    });
    if (!category) {
      throw new Error(`Category with ID ${id} not found`);
    }
    return category.posts;
  }
}
