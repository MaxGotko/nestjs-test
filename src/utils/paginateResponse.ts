export function paginateResponse(data, page, limit) {
  const [result, total] = data;
  const lastPage = Math.ceil(total / limit);
  const nextPage = page + 1 > lastPage ? null : page + 1;
  const prevPage = page - 1 < 1 ? null : page - 1;
  return {
    data: [...result],
    pagination: {
      page,
      perPage: limit,
      nextPage: nextPage,
      prevPage: prevPage,
      lastPage: lastPage,
      totalCount: total,
    },
  };
}
