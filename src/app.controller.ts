import { Controller, Get, Inject, Param, Query, Render } from '@nestjs/common';
import { CategoryService } from './category/category.service';
import { PostService } from './post/post.service';
import { AuthorService } from './autor/author.service';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { Cache } from 'cache-manager';
import { PaginationDto } from './dto/pagination.dto';
import { IPagination } from './interfaces/pagination.interface';
import { PostEntity } from './post/post.entity';

@Controller()
export class AppController {
  constructor(
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    private categoryService: CategoryService,
    private postService: PostService,
    private authorService: AuthorService,
  ) {}

  @Get()
  @Render('index')
  async root(@Query() pagination: PaginationDto) {
    pagination =
      Object.keys(pagination).length === 0
        ? { page: 1, perPage: 10 }
        : pagination;
    const cachedData: IPagination<PostEntity[]> = await this.cacheManager.get(
      'main-page-data',
    );
    if (
      cachedData &&
      pagination &&
      pagination.page === cachedData.pagination.page &&
      pagination.perPage === cachedData.pagination.perPage
    ) {
      return cachedData;
    }
    const news = await this.postService.findAll(pagination);
    await this.cacheManager.set('main-page-data', news, 300000);
    return news;
  }
  @Get('/news/:id')
  @Render('news-details')
  async newsDetails(@Param() params) {
    const post = await this.postService.findOne(params.id);
    return { post };
  }
  @Get('/categories')
  @Render('categories')
  async categories(@Query() pagination: PaginationDto) {
    pagination =
      Object.keys(pagination).length === 0
        ? { page: 1, perPage: 10 }
        : pagination;
    return await this.categoryService.findAll(pagination);
  }
  @Get('/categories/:id')
  @Render('category-details')
  async categoryDetails(@Param() params) {
    const category = await this.categoryService.findOne(params.id);
    const news = await this.categoryService.findNewsByCategoryId(params.id);
    return { category, news };
  }
  @Get('/authors/:id')
  @Render('author-details')
  async authorDetails(@Param() params) {
    const category = await this.authorService.findOne(params.id);
    const news = await this.authorService.findNewsByAuthorId(params.id);
    return { category, news };
  }
}
